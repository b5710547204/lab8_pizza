package pizzashop;
import java.util.*;
import java.util.function.Consumer;

import pizzashop.food.*;

/** A customer order containing food items.
 *  The iterator provides an iterator over the FoodItems in the order.
 *  The Observable notifies observers whenever the order changes.
 *  The FoodOrder itself is attached as an Observer of FoodItems so
 *  it knows when a FoodItem changes.   
 *  @author Patinya Yongyai
 *  @version 10.03.2015
 */
public class FoodOrder implements Iterable {
	/** list of items in the order. */
	private List<Object> items;
	
	/** create a new, empty food order. */
	public FoodOrder() {
		items = new ArrayList();
	}

	/** add an item to the order.
	 *  @param item is the fooditem to add to order	
	 */
	public void add(OrderItem item){
		this.items.add(item);
	}
	
	/** remove an item from the order. 
	 * @param item is item from object
	 * @return item after remove
	 * 
	 */
	public boolean remove(Object item) {
		// return result of ArrayList.remove()
		boolean ok = items.remove( item );
		return ok;
	}
	
	/**
	 * clear item.
	 */
	public void clear() {
		items.clear();
	}
	
	/** 
	 * get the total price of the order.
	 *  @return total price
	 */
	public double getTotal() {
		double price = 0;
		for( Object obj : items ) {
			if (obj instanceof Drink) price += ((Drink) obj).getPrice();
			if (obj instanceof Pizza) price += ((Pizza) obj).getPrice();
		}
		return price;
	}
	
	/** 
	 * return an iterator for the items in the order.
	 * This is easy since ArrayList is already Iterable.
	 * @return iterator of item
	 */
	public Iterator<? extends Object> iterator() {
		return items.iterator();
	}
	
	/**
	 * @return String
	 */
	public String toString() {
		StringBuffer s = new StringBuffer();
		for( Object item : items ) s.append(item.toString()+"\n");
		return s.toString();
	}

}