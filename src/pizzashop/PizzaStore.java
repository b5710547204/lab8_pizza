package pizzashop;
import java.util.*;

/**
 * The store records food orders.
 * @author Patinya Yongyai
 * @version 10.03.2015
 *
 */
public class PizzaStore {
	List<FoodOrder> orders;
	
	// the Singleton object of this class
	private static PizzaStore theStore = null; // the singleton object
	
	private PizzaStore() {
		orders = new ArrayList<FoodOrder>();
	}
	
	/** 
	 * Submit a food order for processing. 
	 * @param fo is food order from customer
	 */
	public void addOrder(FoodOrder fo ) { 
		orders.add( fo ); 
	}
	
	/**
	 *  Remove a food order. 
	 *  @param fo is food order from customer
	 */
	public void removeOrder(FoodOrder fo ) { 
		orders.remove( fo ); 
	}
	
	/** 
	 * Accessor to get the pizza store. 
	 * @return Instance
	 */
	public static PizzaStore getInstance( ) {
		
		// lazy instantiation
		if ( theStore == null ) theStore = new PizzaStore( );
		return theStore;
	}
	
	/** 
	 * List all the food order. 
	 * @return order 
	 */
	public List<FoodOrder> getOrders() {
		return orders;
	}
	
	
}
