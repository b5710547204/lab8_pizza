package pizzashop.food;

/**
 * Reduce duplicate class by using class AbstractItem.
 * @author Patinya Yongyai
 * @version 10.03.2015
 *
 */
public abstract class AbstractItem implements OrderItem{
	/**
	 * 
	 * @param size is size from order
	 * @param prices is prices from order
	 * @return total price
	 */
	public double getPrice(int size, double[] prices){
		double price = 0;
		if ( size >= 0 && size < prices.length ) price = prices[size];
		return price;
	}
}
