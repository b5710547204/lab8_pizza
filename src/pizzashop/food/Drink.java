package pizzashop.food;

/** 
 * A drink has a size and flavor.
 * @author Patinya Yongyai
 * @version 10.03.2015
 */
public class Drink extends AbstractItem{
	// constants related to drink size
	public static final double [] prices = { 0.0, 20.0, 30.0, 40.0 };
	public static final String [] sizes = { "None", "Small", "Medium", "Large" };
	private String flavor;
	private int size;
	/**
	 * create a new drink.
	 * @param size is the size. 1=small, 2=medium, 3=large
	 */
	public Drink( int size ) {
		this.size = size;
	}
	
	/**
	 * @return String description of this pizza.
	 */
	public String toString() {
		return sizes[size] + " " + (flavor==null? "": flavor.toString()) + " drink"; 
	}

	/** return the price of a drink.
	 * @see pizzashop.FoodItem#getPrice()
	 * @return total price
	 */
	public double getPrice() {
		return super.getPrice(size, prices);
	}
	
	/**
	 * Set size.
	 * @param size is size for set size
	 */
	public void setSize(int size){
		this.size = size;
	}
	
	/**
	 * cloned the size and flavor.
	 * @return clone
	 */
	public Object clone() {
		Drink clone;
		
		try {
			clone = (Drink) super.clone();
		} catch (CloneNotSupportedException e) {
			System.err.println("Drink.clone: " + e);
			clone = new Drink(0);
		}
		//superclass already cloned the size and flavor
		return clone;
	}

}
