package pizzashop.food;

/**
 * Which class use this interface,that class must use getPrice() and setSize().
 * @author Patinya Yongyai
 * @version 10.03.2015
 *
 */
public interface OrderItem {
	/**
	 * @return total price
	 */
	public double getPrice();
	/**
	 * 
	 * @param size is size for set size
	 */
	public void setSize(int size);
}
